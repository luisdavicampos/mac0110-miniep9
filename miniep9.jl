function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end
  
function matrix_pot(M, p)
    pot = M
    while p > 1
        pot = multiplica(pot, M)
        p -= 1
    end
    return pot
end

function matrix_pot_by_squaring(M, p)
    square = multiplica(M, M)
    if p > 1
        if p % 2 == 0
            return matrix_pot_by_squaring(square, p/2)
        else
            return multiplica(M, matrix_pot_by_squaring(square, (p-1)/2))
        end
    else
        return M
    end
end

using LinearAlgebra
function compare_times(d, p)
  M = Matrix(LinearAlgebra.I, d, d)
  println("Tempo de matrix_pot:")
  @time matrix_pot(M, p)
  println("Tempo de matrix_pot_by_squaring:")
  @time matrix_pot_by_squaring(M, p)
end

compare_times(30,10)