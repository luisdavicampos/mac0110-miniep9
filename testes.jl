include("miniep9.jl")
using Test

function teste()
    println("Testando...")
    @test pot([1 2 ; 3 4], 1) == [1 2 ; 3 4]
    @test pot([1 2 ; 3 4], 2) == [7.0 10.0 ; 15.0 22.0]
    @test pot([1 0 0; 0 1 0; 0 0 1], 5) == [1 0 0; 0 1 0; 0 0 1]
    @test pot([2 1 ; 0 0], 4) == [16 8 ; 0 0]
    @test pot([2 2 ; 0 0], 5) == [32 32 ; 0 0]
    @test pot([1 2 3 ; 0 -1 -2 ; 1 4 1], 3) == [6 4 -10 ; -2 3 8 ; -2 -14 -2]
    println("Tudo certo, parabéns humano!")
end

function pot(M,p)
    return matrix_pot_by_squaring(M, p)
end

teste()